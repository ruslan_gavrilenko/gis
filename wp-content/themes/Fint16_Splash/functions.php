<?php
/** Functions for Splash **/

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/style.css' );
    wp_enqueue_script( 'child-site', get_stylesheet_directory_uri().'/script/site.js', array('jquery', 'site') );

}


add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() {
?>
<script type="text/javascript">
var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}