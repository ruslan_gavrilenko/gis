<?php get_header(); 

$data = Timber::get_context();
$data['post'] = new TimberPost();
$data['form'] = do_shortcode( '[contact-form-7 id="4" title="Signup"]' );


Timber::render('view/template/page-content.twig', $data);

get_footer(); ?>