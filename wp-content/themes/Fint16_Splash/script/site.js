// Splash Page

(function($){

	var inputFieldToggle = function(){
		var emailInput = $('.email input[type="email"]');
		var personalInfo = $('.personal-info');
		if( emailInput.val() ){
			personalInfo.addClass('active');
		}else{
			personalInfo.removeClass('active');
		}
	}

	var headerScrollSize = function(){
		var scrollTop = $(window).scrollTop(),
			padding = 27 - ( scrollTop/2 ),
			height = 50 - scrollTop,
			paddingVal = " " + padding.toString() + " 20";
		
		if ( padding >= 13) {
			$('header .nav-main-link').each(function(){
				$(this).css({
					"padding-top" : padding
					,"padding-bottom" : padding
				});
			});
		}	

		if ( height >= 25) {
			$('header .logo').css({
				"height": height
			}).removeClass('small');
		} else {
			$('header .logo').css({
				"height": 25
			}).addClass('small');
		}
	}

	var menuToggle = function(){
		var $menu = $('.site-header .mobile-menu');

		$menu.toggleClass('open');
	}

	var showModal = function(type, data){
		var modalTemplate = _.template( $( "."+ type +"-modal-template" ).html() );
		var ajaxData = {
			'action': 'spark_get_attachment_image'
			,'id': parseInt(data.id[1])
		}

		$.ajax({
				url: ajaxUrl,
				data: ajaxData,
				success: function(d){
					console.log(d);
					data.full = $.parseJSON(d);
					$('body').append( modalTemplate(data) );
					$('body').css({'overflow':'hidden'});
					$('.modal .close').click(function(){
						$(this).parents('.modal').remove();
						$('body').removeAttr('style');
					})
				}
			})
	}

	$(document).ready(function(){
		$('.email input').on('input',function(){
			inputFieldToggle();
		})

		$('.menu-button').on('click',function(){
			$(this).toggleClass('open')
			menuToggle();
		})

		$('.page-content img').click(function(){
			var $elem = $(this).parents('.wp-caption');
			var data = {
				id: $elem.attr('id').split('_')
				,img: $(this).attr('src')
				,caption: $elem.find('.wp-caption-text').text()
			}

			showModal('image',data);
		});

		headerScrollSize()
		$(window).scroll(function(){
			headerScrollSize();
		})
	})

})(jQuery);