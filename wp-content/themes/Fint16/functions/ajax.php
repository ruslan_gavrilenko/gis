<?php
// Ajax Functions for Spark Theme

// class Spark_Ajax {

	// Function get_posts()
	function ajax_posts(){
		$args = array(
			"showposts" => $_GET['showposts'] ? $_GET['showposts'] : null
			,"post_type"=> $_GET['post_type'] ? $_GET['post_type'] : 'post'
			,"offset"=> $_GET['page'] ? ( ( $_GET['page'] - 1 ) * $_GET['showposts'] ) + 1 : null
		);

		if ( isset($_GET['cat']) && $_GET['cat'] ){
			$args['cat'] = $_GET['cat'];
		}

		if ( isset($_GET['tag']) && $_GET['tag'] ){
			$args['tag_id'] = $_GET['tag'];
		}

		$query = new WP_Query($args);
		$posts = Timber::get_posts($args);

		foreach( $posts as $key=>$post ){
			$thumbnail = $posts[$key]->thumbnail;
			$posts[$key]->thumbnail = (array) $thumbnail;

			$posts[$key]->logo = new TimberImage($posts[$key]->logo);
			$posts[$key]->logo = (array) $posts[$key]->logo;

			$posts[$key]->post_date = date('m.d.Y', strtotime($posts[$key]->post_date));
			$posts[$key]->media_post_date = date('m.d.Y', strtotime($posts[$key]->media_post_date));
		}

		$data = array(
			"posts"  => $posts
			,"query" => $query
		);

		$data = wp_json_encode($data);
		echo $data;
		die(0);
	}
	add_action( 'wp_ajax_spark_get_posts', 'ajax_posts' );
	add_action( 'wp_ajax_nopriv_spark_get_posts', 'ajax_posts' );

	// Function get_posts()
	function ajax_attachment_image(){
		$args = array(
			"id" => $_GET['id'] ? $_GET['id'] : null
		);

		$data = json_encode(wp_get_attachment_image_src( $args['id'], 'full' ));
		echo $data;
		die(0);
	}
	add_action( 'wp_ajax_spark_get_attachment_image', 'ajax_attachment_image' );
	add_action( 'wp_ajax_nopriv_spark_get_attachment_image', 'ajax_attachment_image' );
// }