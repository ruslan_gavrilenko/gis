(function($){

    $.fn.getExcerpt = function(string,length,more_txt) {
		string = string.replace('/< /?[^>]+>/gi', ''); //replace html tags
		string = jQuery.trim(string);  //trim whitespace
        // string = string.split(' ').slice(0,500).join(' '),
		string = string.substring(0,length)+more_txt;  //update the html on page
		return string; //allow jQuery chaining 
	}

    PostAjaxPager = Backbone.View.extend({
        el: '.post-ajax-pager'
        ,events: {
            'click button.load-more' : 'loadMore'
        }
        ,initialize: function(options){
             _.extend(this, _.pick(options, 'tile_template', 'modal_template', 'post_type', 'page_count', 'cat', 'tag'));
            this.options = options;
            this.options.cat = options.cat ? options.cat : this.$el.data("category");
            this.options.isotope = this.options.isotope ? this.options.isotope : false
            this.options.page_count = options.page_count ? options.page_count : this.$el.data("page-count");
            this.options.page_current = options.page_current ? options.page_current : 1;
            this.options.tag = options.tag ? options.tag : this.$el.data("tag");
            this.options.tile_template = this.options.tile_template ? this.options.tile_template : _.template($('#archive-template').html());
            // this.options.modal_template = this.options.modal_template ? this.options.modal_template : _.template($('#archive-template').html());
            this.options.post_type = this.post_type ? this.post_type : "post";

            if( this.options.isotope ){
                this.$el.find('.list').isotope({
                    layoutMode: 'packery'
                    // ,itemSelector: '.client'
                });
            }


        }
        ,loadMore: function(){
            var _this = this;
            $.ajax({
                url: myAjax.ajaxurl
                ,data:{
                    action: "spark_get_posts"
                    ,type: "post"
                    ,showposts: 10
                    ,post_type: _this.options.post_type ? _this.options.post_type : 'post'
                    ,page: _this.options.page_current + 1
                    ,cat: _this.options.cat ? _this.options.cat : null
                    ,tag: _this.options.tag ? _this.options.tag : null
                }
                ,success: function( data ){
                    data = JSON.parse(data);
                    $.each(data.posts, function(i, item){
                        item.date = item.display_date;
                        item.summary = $().getExcerpt(item.post_content, 800, '' );
                        
                        var elem = $(_this.options.tile_template(item));
                        
                        _this.$el.find('.list').append(elem)
                        if ( _this.options.isotope ){
                            _this.$el.find('.list').isotope('appended',elem);
                        }
                        _this.options.loaded = true;
                    });
                    _this.options.page_current++;
                    
                    if ( _this.options.page_current >= data.query.max_num_pages ){
                        _this.$el.find('.load-more').css('display','none');
                    }
                }
                ,error: function(e){
                    console.log(e);
                }
            })            
        }
        ,selectFilter: function(filter){
            this.$el.isotope({ filter: filter });
        }
    });

    PostLoadedPager = Backbone.View.extend({
        el: '.post-loaded-pager'
        ,events: {
            'click button.load-more' : 'showMorePosts'
            ,'click li.client': 'loadModal'
        }
        ,initialize: function(options){
            var _this = this;

             _.extend(this, _.pick(options, 'tile_template', 'modal_template', 'post_type', 'page_count', 'cat', 'tag'));
            this.options = options;
            this.options.cat = options.cat ? options.cat : this.$el.data("category");
            this.options.list = options.list ? options.list : this.$el.find('.list').data('feed');
            this.options.page_count = options.page_count ? options.page_current : this.$el.data("page-count");
            this.options.page_current = options.page_current ? options.page_current : 1;
            this.options.post_type = this.post_type ? this.post_type : "post";
            this.options.tag = options.tag ? options.tag : this.$el.data("tag");
            this.options.tile_template = this.options.tile_template ? this.options.tile_template : _.template($('#archive-template').html());
            this.options.modal_template = this.options.modal_template ? this.options.modal_template : _.template($('#archive-template').html());
            
            $.each( this.options.list, function( i, item ){
                _this.options.list[i].index = i;
            })

            if( this.options.isotope ){
                this.$el.find('.list').isotope({
                    layoutMode: 'packery'
                    ,itemSelector: '.client'
                });
            }



            $('.filter-selector li').unbind().on('click', function(){
                var filter = $(this).data('filter');
                window.location.hash = '#' + filter.replace(".", "");
                _this.showMorePosts(false, filter);
            })

            this.doHash(window.location.hash.replace(/^.*?(#|$)/,''));
        }

        ,doHash: function(hash){
            var _this = this;

            if ( hash !== "" ){
                var $hash = $('[data-anchor="'+hash+'"]');
                

                if( $hash.hasClass('sector')  ){
                    _this.showMorePosts(false, '.'+hash);
                } else {
                    if ( !$hash.length ){
                        _this.showMorePosts(false, '*');
                        $hash = $(hash);
                    }
                    _this.showModal($hash);
                }
            }
        }

        ,showMorePosts: function(e, filter){
                var _this = this;
                this.options.index = this.options.index ? this.options.index : this.$el.find('.list').data('last-index');
                var group = filter ? this.options.list.slice(this.options.index) : this.options.list.slice(this.options.index, this.options.index + 11);
                var groupElem = "";

                this.options.index += 11;

                $.each(group, function(i, item){
                    
                    item.sector_tag = item.sector
                                        .toLowerCase()
                                        .replace("&","")
                                        .replace("!","")
                                        .split(' ').join('-');

                    item.name_tag = item.name
                                        .toLowerCase()
                                        .replace("&","")
                                        .replace("!","")
                                        .split(' ').join('-');

                    item.data = "";//$('<div/>').text(JSON.stringify(item)).html();

                    groupElem += _this.tile_template(item);
                });

                $groupElem = $(groupElem);

                _this.$el.find('.clients.list')
                    .append($groupElem)
                    .isotope('appended',$groupElem);

                this.$el.find('.client.no-modal').on('click', function(){
                    _this.showModal($(this));
                    $(this).removeClass('no-modal');
                });

                if ( typeof filter != 'undefined' ){
                    _this.$el.find('.clients').isotope({ filter: filter });
                    _this.$el.find('.load-more').css('display', 'none');
                } else {
                    _this.$el.find('.clients').isotope({ filter: '*' });  
                    if(_this.options.index > _this.options.list.length){
                        _this.$el.find('.load-more').css('display', 'none');
                    }
                    
                }

        } 
        ,loadModal: function(data){

            if ( data.currentTarget ){
                var index = $(data.currentTarget).data('index');
                data = this.options.list[index];
            } 

            var _this = this;
            var index = data.index;

            data.client_tag = data.name
                                .toLowerCase()
                                .replace(" & "," ")
                                .replace("!","")
                                .split(' ').join('-');

            data.sector_tag = data.sector
                                .toLowerCase()
                                .replace(" & "," ")
                                .replace("!","")
                                .split(' ').join('-')
                                .replace('--','-');



            data.prevItem = this.options.list[index-1] ? this.options.list[index-1] : undefined;
            if ( typeof data.prevItem !== 'undefined' ){
                data.prevItem.index = index-1;
                data.prevItem.client_tag = data.prevItem.name
                                    .toLowerCase()
                                    .replace(" & "," ")
                                    .replace("!","")
                                    .split(' ').join('-');
            }

                data.nextItem = this.options.list[index+1] ? this.options.list[index+1] : undefined ;
            if ( typeof data.nextItem !== 'undefined' ){
                data.nextItem.index = index+1;
                data.nextItem.client_tag = data.nextItem.name
                                    .toLowerCase()
                                    .replace(" & "," ")
                                    .replace("!","")
                                    .split(' ').join('-');
            }

            $('body').append(this.modal_template(data));
            $('.'+ data.client_tag + '.client-modal').find('.pager span').on('click', function(){
                var client = $(this).data('client');
                var index = $(this).data('index');

                if( client && $('.' + client + '.client-modal').length > 0 ){
                    _this.showModal($('#'+client+'.client'));
                } else {
                    _this.options.list[index].index = index;
                    _this.loadModal(_this.options.list[index]);
                }
                
                $(this).parents('.client-modal').remove();
            })

            $('.'+ data.client_tag + '.client-modal').find('.close').on('click', function(){
                $(this).parents('.client-modal').removeClass('open');
            });

            window.location.hash = '#' + data.client_tag.replace(".", "");
        }   
        ,showModal: function($elem){
            var _this = this;
            var index = $elem.data('index');
            var data = this.options.list[ index ];

            data.client_tag = data.name
                                .toLowerCase()
                                .replace("&","")
                                .replace("!","")
                                .split(' ').join('-');

            data.sector_tag = data.sector
                                .toLowerCase()
                                .replace(" & "," ")
                                .replace("!","")
                                .split(' ').join('-')
                                .replace('--','-');

            data.prevItem = this.options.list[index-1] ? this.options.list[index-1] : undefined;
            data.prevItem.index = index-1;
            data.prevItem.client_tag = data.prevItem.name
                                .toLowerCase()
                                .replace("&","")
                                .replace("!","")
                                .replace("/","-")
                                .split(' ').join('-');

            data.nextItem = this.options.list[index+1] ? this.options.list[index+1] : undefined;
            data.nextItem.index = index+1;
            data.nextItem.client_tag = data.nextItem.name
                                .toLowerCase()
                                .replace("&","")
                                .replace("!","")
                                .replace("/","-")
                                .split(' ').join('-');

            $('body').append(this.modal_template(data));
            $('.'+ data.client_tag + '.client-modal').find('.pager span').on('click', function(){
                var client = $(this).data('client');
                var index = $(this).data('index');

                if( $('.' + client + '.client-modal').length > 0 ){
                    _this.showModal($('#'+client+'.client-modal'));
                } else {
                    _this.loadModal(_this.options.list[index]);
                }
                
                $(this).parents('.client-modal').remove;
            })

            $('.'+ data.client_tag + '.client-modal').find('.close').on('click', function(){
                $(this).parents('.client-modal').remove;
            });

        }      
    });

    PostLoader = Backbone.View.extend({
        el: '.post-loader'

        ,events: {
            'click button.load-more': 'showMore'
        }

        ,initialize: function(options){
        	 _.extend(this, _.pick(options, 'template', 'template_tile', 'post_type', 'page_count', 'cat', 'tag'));
            this.options = options;
            this.options.list = options.list ? options.list : this.$el.find('.list').data('feed');
            this.options.page_count = options.page_count ? options.page_current : this.$el.data("page-count");
            this.options.page_current = options.page_current ? options.page_current : 1;
            this.options.cat = options.cat ? options.cat : this.$el.data("category");
            this.options.tag = options.tag ? options.tag : this.$el.data("tag");
            this.template = this.template ? this.template : _.template($('#archive-template').html());
            this.template_tile = this.template_tile ? this.template_tile : _.template($('#archive-template').html());
            this.options.post_type = this.post_type ? this.post_type : "post";

            $('.who-we-finance .filter-selector li').unbind().on('click', function(){
                var filter = $(this).data('filter');
                if ( ! this.options.loaded ){
                    this.loadMore(filter);
                } else {
                    _this.selectFilter(filter)
                }
            })

        }
        ,showMore: function(){
            if ( this.$el.hasClass('loaded-list')){
                this.showMorePosts();
            }else {
                this.loadPosts();
            }
        }
        ,showMorePosts: function(){
            var _this = this;
            this.options.index = this.options.index ? this.options.index : this.$el.find('.list').data('last-index');
            var group = this.options.list.slice(this.options.index, this.options.index + 10);
            var groupElem = "";
            this.options.index += 10;

            $.each(group, function(i, item){
                item.sector_tag = item.sector
                                    .toLowerCase()
                                    .replace("&","")
                                    .replace("!","")
                                    .split(' ').join('-');

                item.name_tag = item.name
                                    .toLowerCase()
                                    .replace("&","")
                                    .replace("!","")
                                    .split(' ').join('-');

                item.data = JSON.stringify(item);

                groupElem += _this.template_tile(item);
            });

            _this.$el.find('.list').append(groupElem).isotope('addItems',groupElem);

        }
        ,loadPosts: function(){
            var _this = this;
            $.ajax({
                url: myAjax.ajaxurl
                ,data:{
                    action: "spark_get_posts"
                    ,type: "post"
                    ,showposts: 10
                    ,post_type: _this.options.post_type ? _this.options.post_type : 'post'
                    ,page: _this.options.page_current + 1
                    ,cat: _this.options.cat ? _this.options.cat : null
                    ,tag: _this.options.tag ? _this.options.tag : null
                }
                ,success: function( data ){
                    data = JSON.parse(data);
                    data.query.page_count = Math.ceil( parseInt(data.query.found_posts) / data.query.query.showposts );
                   
                    $.each(data.posts, function(i, item){
                        item.date = item.display_date;
                        item.excerpt = $().getExcerpt(item.post_content, 90, '' );
                        _this.$el.find('.posts').append(_this.template(item));
                    });
                    _this.options.page_current++;
                    
                    if ( _this.options.page_current >= data.query.page_count ){
                        _this.$el.find('.load-more').css('display','none');
                    }
                }
                ,error: function(){
                    console.log('ERROR!');
                }
            })    
        }
    });


})(jQuery);