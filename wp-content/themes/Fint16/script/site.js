(function($){
	$(document).ready(function(){

		$('.slideout-search .switch, .slideout-search .close').on('click', function(){
			var searchbar = $(this).parent().parent().parent();
			searchbar.toggleClass('open');
			if ( searchbar.hasClass('open') ){
				setTimeout(function(){
					searchbar.find('input[type="text"]').focus();
				}, 500);
			}
			$('body').keyup(function(e){
			    if(e.keyCode == 27){
			        searchbar.removeClass('open');
			    }
			});
		})

		$('body').click(function(e)
		{
		   $('div.header-sub').removeClass('open');
		});
		$('div.header-sub').click(function(e)
		{
		   e.stopPropagation();
		});

		$('.sign-in-button').click(function(){
			$('.sign_on_box.header').toggleClass('open');
			$('body').css('overflow', 'hidden');
		})
		$('.sign_on_box.header .close').click(function(){
			$('body').removeAttr('style');
		})

	})


})(jQuery)