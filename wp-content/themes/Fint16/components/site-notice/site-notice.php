<?php
	//Site Notice Post Type
	$label = array(
		'name'	=>	'Site Notice'
		,'singular_name'	=> 'Media'
	);

	$noticeArgs = array(
		'labels'	=>	$label
		,'public'	=>	true
		,'supports'	=>	array('title', 'editor')
		,'rewrite'  => array( 'slug' => 'site_notice' )
	);

	register_post_type( 'Site Notice', $noticeArgs );	


	function site_notice_enqueue() {
		wp_enqueue_script( 'site-notice-js', get_template_directory_uri()."/components/site-notice/script/site-notice.js", array('jquery') );
		wp_enqueue_style( 'site-notice-css', get_template_directory_uri()."/components/site-notice/site-notice.css" );
	}

	add_action('wp_enqueue_scripts', 'site_notice_enqueue');