(function($){

	$(window).load(function() {
		$.expr[":"].external = function(a) {
			return !a.href.match(/^mailto\:/) && !a.href.match(/^tel\:/) && a.hostname != location.hostname

		};
        // OPEN 3RD PARTY SITE IN NEW TAB/WINDOW
        $('#url_link').click(function() {
        	window.open(this.href);
        	return false;
        });
        // ADD BOOTSTRAP DATA-TOGGLE ATTRIBUTE TO THE LINKS
        $('a:not(#url_link):external').attr('data-toggle', 'modal');
        $('a:not([href="#"]):not(#url_link):external').attr('data-target', '#speedbump');
        $("a:not([href~='javascript']):not([class~='twitter']):not([class~='facebook']):not([class~='linkedin']):not([href='#']):not(.addthis_button_compact):not(#url_link):external").addClass("ext_link");
        	
    	$('a.ext_link').click(function(e) {
    		e.preventDefault();
            var exclude = $('.speedbump-exclude-list').data('exclude');
                exclude = exclude.split(',');

            var url = $(this).attr('href');
            var include = $.inArray(url, exclude) >= 0 ? false: true;
            var title = $(this).attr('title') ? $(this).attr('title') : $(this).html();
            
            $('#url_link').attr('href', url);
            $('#url_link').find('.link-address').html(title);
            $('h3#speedbump-title').replaceWith('<h3 id="speedbump-title">' + title + '</h3>');

            if ( include ){
                $('#speedbump.modal').removeClass('closed').addClass('external');

                $('#speedbump.modal .close, #speedbump.modal .dismiss, #url_link').click(function() {
                    $('#speedbump').addClass('closed').setTimeout(function(){
                        $(this).removeClass('external')
                    }, 500);
                }); // DESTROY MODAL IF USER CONTINUES                
            } else {
                $('#url_link').click();
            }

    	});

        $('a[href^="mailto"]:not(#url_link)').click(function(e){
            e.preventDefault();
            var url = $(this).attr('href');

            $('#email-link').find('#url_link').attr('href', url).find('.email-address').text(url.substring(7));
            $('#speedbump.modal').removeClass('closed').addClass('email');

            $('#speedbump.modal .close, #speedbump.modal .dismiss, #url_link').click(function() {
                $('#speedbump').addClass('closed').removeClass('email');
            });
        })
    });


})(jQuery)