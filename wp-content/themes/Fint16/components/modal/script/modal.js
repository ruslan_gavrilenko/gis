(function($){

	closeModal = function($elem){
		$elem.removeClass('open');
	}

	$('.modal-trigger').on('click', function(){
		var $modal = $($(this).attr('modal-class'));
		$('body').css('overflow', 'hidden');
		$modal.removeClass('closed');
	});

	$('.modal.open .close').on('click', function(){ closeModal($(this).parent()); });


})(jQuery)