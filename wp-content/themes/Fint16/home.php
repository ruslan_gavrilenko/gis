<?php get_header();

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['form'] = do_shortcode('[contact-form-7 id="4" title="Signup"]');
Timber::render('view/splash/content_with_form.twig', $context);

get_footer();